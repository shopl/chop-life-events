
document.addEventListener('DOMContentLoaded', () => {


	var active_tab = document.querySelector('.tab-titles .tab-title:first-child');
    active_tab.classList.add("active");
	var active_category = active_tab.getAttribute("data-id");
	var category_name = '[data-category="' + active_category+'"]';
	var items = document.querySelectorAll(category_name);
	items.forEach(item => {
		item.classList.remove("hidden");
	});
	
	var tab_titles = document.getElementsByClassName("tab-title");

	var updateFaq = function() {
		var all_title = document.querySelectorAll('.tab-title');
		all_title.forEach(item => {
			item.classList.remove("active");
		});
		this.classList.add("active");
		var all_faq = document.querySelectorAll('.roster-item')
		all_faq.forEach(item => {
			item.classList.add("hidden");
		});
		var active_category = this.getAttribute("data-id");
		var category_name = '[data-category="' + active_category+'"]';
		var items = document.querySelectorAll(category_name);
		items.forEach(item => {
			item.classList.remove("hidden");
		});
	};

	for (var i = 0; i < tab_titles.length; i++) {
		tab_titles[i].addEventListener('click', updateFaq, false);
	}

    var roster_buttons = document.querySelectorAll('.roster-in');

    var modalPopup = function(){
		modalClose();
        var roster = this.parentNode;
        var modal = roster.querySelectorAll('.roster-modal');
		roster.querySelector('.modal_close').classList.remove("hidden");
        for (var i = 0; i < modal.length; i++) {
            modal[i].classList.remove("hidden");
        }
        
    }
	for (var i = 0; i < roster_buttons.length; i++) {
		
		roster_buttons[i].addEventListener('click', modalPopup, false);
		
	}

	var modal_close = document.querySelectorAll('.modal_close');
	var modalClose = function(){
		var close_btn = document.querySelectorAll('.modal_close');
		for (var i = 0; i < close_btn.length; i++) {
            close_btn[i].classList.add("hidden");
        }	
		var roster_moal = document.querySelectorAll('.roster-modal');
		for (var i = 0; i < roster_moal.length; i++) {
            roster_moal[i].classList.add("hidden");
        }	
	}
	for (var i = 0; i < modal_close.length; i++) {
		modal_close[i].addEventListener('click', modalClose, false);
	}
});